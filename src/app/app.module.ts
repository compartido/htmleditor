import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

//import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CKEditorModule } from 'ng2-ckeditor';

import { CkEditorHtmlComponent } from './pages/ck-editor-html/ck-editor-html.component';
import { CkEditor4HtmlComponent } from './pages/ck-editor4-html/ck-editor4-html.component';

@NgModule({
  declarations: [
    AppComponent,
    CkEditorHtmlComponent,
    CkEditor4HtmlComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CKEditorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
