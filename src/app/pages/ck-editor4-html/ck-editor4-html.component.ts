import { Component, OnInit } from '@angular/core';
import { Ckeditor4ConfigService } from 'src/app/services/ckeditor4-config.service';

@Component({
  selector: 'app-ck-editor4-html',
  templateUrl: './ck-editor4-html.component.html',
  styleUrls: ['./ck-editor4-html.component.css'],
})
export class CkEditor4HtmlComponent implements OnInit {
  ckeditorContent: string = '<p>Some html</p>';
  private myCkeditor4Config: any;
  constructor(private ckService: Ckeditor4ConfigService) {}

  ngOnInit() {
    this.myCkeditor4Config = this.ckService.getConfig(150,400);
  }

}
